package org.SIDIBE.Serie9.exo18;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AnalyzeBean anaBean=new AnalyzeBean();
		//question 1
		Object p=new Person();
		Object e=new Employee();
		Object p1;
		System.out.println(anaBean.getClassName(p));
		//question 2
		p1=(Person)anaBean.getInstance("org.SIDIBE.Serie9.exo18.Person");
		System.out.println(p1);
		//question 3
		System.out.println(anaBean.getProperties(p));
		System.out.println(anaBean.getProperties(e));
		//question 4
		p1=new Person("yuss","sidibe",20);
		System.out.println("L'age de yuss est: "+anaBean.get(p1, "age"));
		//question 5
		Person p2=new Person();
		System.out.println("Modification du champs firstName de Person p2");
		anaBean.set(p2, "firstName", "Mariama");
		System.out.println("Le nouveau pr�nom de la personne p2 est : "+p2.getFirstName());
		//question 6
		List<Object> beans=anaBean.read("files/test.txt");
		System.out.println(beans);
		//question 7
		beans.add(new Person("Aissatou","Sidibe",21));
		anaBean.write(beans, "files/testWrite.txt");
		//v�rifions si on peut utiliser le fichier qu'on vient d'�crire pour tester la m�thode read() de la question 6
		System.out.println(anaBean.read("files/testWrite.txt"));
		
	}

}