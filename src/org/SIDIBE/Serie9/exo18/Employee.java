package org.SIDIBE.Serie9.exo18;

public class Employee extends Person{
	private int salary;
	
	public Employee(){};
	public Employee(int salaire){
		this.setSalary(salaire);
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Employee [firstName=" + super.getFirstName()
				+ ", lastName=" + super.getLastName() + ", age=" + super.getAge()+ " ,salary=" + salary+"]";
	}
}