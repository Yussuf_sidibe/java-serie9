package org.SIDIBE.Serie9.exo18;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AnalyzeBean {
	public String getClassName(Object o) {
		return o.getClass().getName();
	}

	public Object getInstance(String className) {
		Object instance = null;
		Class<?> clss;
		try {
			clss = Class.forName(className);
			instance = clss.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return instance;
	}

	public List<String> getProperties(Object o) {
		List<String> list = new ArrayList<>();
		Function<String, String> nomChamps = s -> s.substring(3, 4).toLowerCase() + s.substring(4, s.length());
		try {
			Class<?> clss = Class.forName(this.getClassName(o));
			list = Arrays.stream(clss.getMethods()).map(method -> method.getName())
					.filter(chaine -> chaine.startsWith("get")).map(nomChamps).collect(Collectors.toList());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return list;

	}

	public Object get(Object bean, String property) {
		Object object = null;
		String chaineGet = "get" + property.substring(0, 1).toUpperCase() + property.substring(1, property.length());
		try {
			Method getter = bean.getClass().getMethod(chaineGet);
			try {
				object = getter.invoke(bean);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return object;
	}

	public void set(Object bean, String property, Object value) {
		String chaineSet = "set" + property.substring(0, 1).toUpperCase() + property.substring(1, property.length());
		String chaineGet = "get" + property.substring(0, 1).toUpperCase() + property.substring(1, property.length());
		try {

			Method getter = bean.getClass().getMethod(chaineGet);
			Method setter = bean.getClass().getMethod(chaineSet, getter.getReturnType());
			try {
				setter.invoke(bean, value);
			} catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SecurityException | NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Object> read(String fileName) {

		// declaration de notre FileReader a l'exetieur des
		// blocs try {} catch {}

		List<Object> list = new ArrayList<>();
		Object o = null;
		String[] champs;
		try (FileReader fr = new FileReader(fileName); BufferedReader bf = new BufferedReader(fr);) {

			// ouverture d'un flux sur le flux de lecture du fichier
			String line = null;
			String property;
			String value = "";
			do {
				line = bf.readLine();
				if (line != null) {
					if (line.charAt(0) != '#') {
						if (line.startsWith("bean")) {
							System.out.println("Ajout d'un nouveau bean!!!");
							if (o != null)
								list.add(o);
						} else {
							champs = line.split("=");
							if (champs[0].substring(3, champs[0].length()).equals("class"))

								o = this.getInstance(champs[1]);
							else {
								property = champs[0].substring(3, champs[0].length());
								value = champs[1];
								if (property.equals("age") || property.equals("salary"))
									this.set(o, property, Integer.parseInt(value));
								else
									this.set(o, property, value);
							}
						}
					}
				}
			} while (line != null);
			if (o != null)
				list.add(o);
			// quelques informations sur la lecture
		} catch (IOException e) {

			// gestion de l'erreur
			System.out.println("Erreur " + e.getMessage());
			e.printStackTrace();
		}
		return list;
	}

	public void write(List<Object> beans, String fileName) {
		String chaine = "p";
		List<String> champs;
		Object cls;
		try (FileWriter fw = new FileWriter(fileName); BufferedWriter bw = new BufferedWriter(fw);) {
			for (int i = 0; i < beans.size(); i++) {
				bw.write("bean.name=" + chaine + (i + 1) + "\n");
				champs = this.getProperties(beans.get(i));
				for (int j = 0; j < champs.size(); j++) {
					if (champs.get(j).equals("class")) {
						cls = this.get(beans.get(i), champs.get(j)).toString();
						bw.write(chaine + (i + 1) + "." + ((String) cls).replaceAll(" ", "=") + "\n");
					}
				}
				for (String prop : champs) {
					if (!prop.equals("class"))
						bw.write(chaine.concat((i + 1) + "." + prop + "=" + this.get(beans.get(i), prop)) + "\n");
				}
			}
		} catch (IOException e) {
			System.out.println("Erreur " + e.getMessage());
			e.printStackTrace();
		}
	}

}
